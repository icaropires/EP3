class AddDetailsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :curso, :string
    add_column :users, :ira, :float
    add_column :users, :semestre_inicio, :string
  end
end
