class CreateDisciplinas < ActiveRecord::Migration[5.0]
  def change
    create_table :disciplinas do |t|
      t.string :nome
      t.string :mencao
      t.string :pre_requisito
      t.integer :creditos
      t.string :categoria
      t.string :semestre

      t.timestamps
    end
  end
end
