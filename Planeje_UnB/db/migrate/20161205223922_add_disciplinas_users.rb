class AddDisciplinasUsers < ActiveRecord::Migration[5.0]
  def self.up
  	create_table :disciplinas_users, :id => false do |t|
  		t.integer :disciplina_id
  		t.integer :user_id
    end
  end

  def self.down
  	drop_table :disciplinas_users
  end
end
