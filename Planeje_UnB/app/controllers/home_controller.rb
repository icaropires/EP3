class HomeController < ApplicationController
	skip_before_action :verify_authenticity_token

  @@obrigatorias = 0
  @@optativas = 0
  @@ira = 0
  @@porcentagem = 0
  @@previsao = 0

  def home
    @obrigatorias = @@obrigatorias
    @optativas = @@optativas
    @ira = @@ira
    @porcentagem = @@porcentagem
    @previsao = @@previsao
  end
  
  def semestre
  	@disciplina_selecionada = []
  	@mencao_selecionada = []

  	@operacao_add = []
  	@operacao_sub = []

    @@obrigatorias = params[:obrigatorias].to_i
    @@optativas = params[:optativas].to_i

    @btn_ok = params[:ok]

  	@disciplina_selecionada << params[:disciplina_1] << params[:disciplina_2] << params[:disciplina_3] << params[:disciplina_4] << params[:disciplina_5] << params[:disciplina_6] << params[:disciplina_7] 
  	@disciplina_selecionada << params[:disciplina_8] << params[:disciplina_9] << params[:disciplina_10] << params[:disciplina_11] << params[:disciplina_12] << params[:disciplina_13] << params[:disciplina_14]

  	@mencao_selecionada << params[:mencao_1] << params[:mencao_2] << params[:mencao_3] << params[:mencao_4] << params[:mencao_5] << params[:mencao_6] << params[:mencao_7] 
  	@mencao_selecionada << params[:mencao_8] << params[:mencao_9] << params[:mencao_10] << params[:mencao_11] << params[:mencao_12] << params[:mencao_13] << params[:mencao_14]

  	@operacao_add << params[:add_1] << params[:add_2] << params[:add_3] << params[:add_4] << params[:add_5] << params[:add_6] << params[:add_7]
  	@operacao_add << params[:add_8] << params[:add_9] << params[:add_10] << params[:add_11] << params[:add_12] << params[:add_13] << params[:add_14]  #operação para adicionar nova disciplina
  	
  	@operacao_sub << params[:sub_1] << params[:sub_2] << params[:sub_3] << params[:sub_4] << params[:sub_5] << params[:sub_6] << params[:sub_7]
  	@operacao_sub << params[:sub_8] << params[:sub_9] << params[:sub_10] << params[:sub_11] << params[:sub_12] << params[:sub_13] << params[:sub_14]


    if @btn_ok == nil
    	#verifica em qual card houve interação
    	@card = nil
    	for valido in @operacao_add
    		if valido != nil
    			@card = @operacao_add.index(valido) + 1
    			break
    		end
    	end
    	if @card == nil
    		for valido in @operacao_sub
    			if valido != nil
    				@card = @operacao_sub.index(valido) + 1
    				break
    			end
    		end
    	end

    	@operacao_add = @operacao_add[@card - 1]
    	@operacao_sub = @operacao_sub[@card - 1]
    	@disciplina_selecionada = @disciplina_selecionada[@card - 1]
    	@mencao_selecionada = @mencao_selecionada[@card - 1]

    	# conta créditos
    	@soma_creditos = 0
    	for materia in current_user.disciplinas.all
    		if materia.semestre == "1" and materia.mencao.to_i >= 3
    			@soma_creditos += materia.creditos
    		end
    	end

    	#remove disciplina selecionada
    	if @operacao_add == nil and @disciplina_selecionada != nil 
    		@disciplina_selecionada = @disciplina_selecionada.to_i
    		@verifica_nome = Disciplina.find(@disciplina_selecionada).nome
    		for materia in current_user.disciplinas.all
    			if materia.nome.include? @verifica_nome and materia.semestre == @card.to_s
    				current_user.disciplinas.find(materia.id).destroy
    			end
    		end
    		redirect_to "/"

    	#adiciona disciplina
    	elsif @operacao_sub == nil and @disciplina_selecionada != nil and @mencao_selecionada != nil and @soma_creditos <= 32 
    		@disciplina_selecionada = @disciplina_selecionada.to_i
    		@mencao_selecionada = @mencao_selecionada.to_i
    		@verifica_nome = Disciplina.find(@disciplina_selecionada).nome
    		@pre_requisito = Disciplina.find(@disciplina_selecionada).pre_requisito.split(',')
    		@pre_requisito_ok = nil
    		@contador = 0

    		#verifica se ele fez os pré requisitos em semestres diferentes do atual
    		for i in @pre_requisito
    		 	for materia in current_user.disciplinas.all
    				if materia.nome.include? i and materia.semestre.to_i < @card
  	  		 		@contador = @contador + 1
    		 			break
    		 		end
    		 	end
    		end

    	    (@contador == @pre_requisito.size)? @pre_requisito_ok = true : @pre_requisito_ok = false
    	    @contador = 0

    	    #verifica se pegou a matéria e passou
    		for materia in current_user.disciplinas.all
    			if materia.nome.include? @verifica_nome and (materia.mencao.to_i >= 3 or materia.semestre.include? @card.to_s)
    				@contador = 1
    				break
    			end
    		end

    		#verifica se ele não passou e os pre requisitos estão ok
    		if @contador != 1 and @pre_requisito_ok == true
    			@temp = Disciplina.find(@disciplina_selecionada).amoeba_dup #auxiliar na alteração do nome da matéria que pertencerá ao usuário
  			@temp.update(nome: @temp.nome + '*' + current_user.email + '_' + @card.to_s, mencao: @mencao_selecionada, semestre: @card.to_s)
  			current_user.disciplinas << @temp
  			current_user.save
  			redirect_to "/"
    		end
    	end

    	@operacao_add = nil
    	@operacao_sub = nil
    else
      @@obrigatorias = params[:obrigatorias].to_f
      @@optativas = params[:optativas].to_f

      if current_user.disciplinas.size != 0
        aux = 0.0
        aux2 = 0.0
        aux_porcentagem = 0.0
        aux_entrada = current_user.semestre_inicio.split('/')
        maior_semestre = 0

        @@ira = (1.0 - (0.6 * @@obrigatorias + 0.4 * @@optativas)/current_user.disciplinas.size)

        for materia in current_user.disciplinas.all
          aux = aux + (materia.mencao.to_f * materia.creditos * materia.semestre.to_f)
          aux2 = aux2 + (materia.creditos * materia.semestre.to_f)
          if materia.mencao.to_i >= 3
            aux_porcentagem = aux_porcentagem + materia.creditos
          end
          if materia.semestre.to_i > maior_semestre
            maior_semestre = materia.semestre.to_i
          end
        end
        @@ira *= aux/aux2

        @@porcentagem = (aux_porcentagem/240.0)*100.0

        @@previsao = (240 - aux_porcentagem)/(10 - maior_semestre)

      end
      redirect_to "/"
    end
  end

end
