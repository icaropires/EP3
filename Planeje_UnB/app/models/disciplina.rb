class Disciplina < ApplicationRecord
	serialize :pre_requisito
	serialize :semestre

	has_and_belongs_to_many :users

	validates_uniqueness_of :nome, :message => "já existente"
	validates_presence_of :nome, :creditos, :categoria, message: 'não pode ser deixado em branco'
	validates_length_of :creditos, in: 1..2, message: "fora do intervalo permitido"
	validates_numericality_of :creditos, message: "deve ser numero"
end
