json.extract! disciplina, :id, :nome, :mencao, :pre_requisito, :creditos, :categoria, :created_at, :updated_at
json.url disciplina_url(disciplina, format: :json)