Rails.application.routes.draw do

  devise_for :users
  resources :disciplinas


  devise_scope :user do
  	authenticated :user do
    	root 'home#home', as: :authenticated_root
  	end

  	unauthenticated do
    	root 'devise/sessions#new', as: :unauthenticated_root
  	end
  end

  post "semestre" => "home#semestre"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
