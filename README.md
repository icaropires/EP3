# EP3 - Orientação à objetos (UnB - Gama)

Este projeto consiste em um website feito Ruby on Rails para auxiliar no gerenciamento de cursos da UnB (inicialmente engenharia de software) para alunos ou curiosos

# Recursos
	-O usuário pode registrar quais matérias fez em quais semestres
	-Obter estatatísticas como:
		-IRA
		-Porcentagem concluída do curso
		-Média de créditos que terá que pegar por semestre para terminar o curso no prazo
	
# Sobre o desenvolvimento
Rails 5.0.0.1
Ruby 2.3.1

